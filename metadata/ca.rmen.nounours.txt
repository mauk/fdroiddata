Categories:Games
License:GPLv3
Web Site:https://github.com/caarmen/nounours-android/blob/HEAD/README.md
Source Code:https://github.com/caarmen/nounours-android
Issue Tracker:https://github.com/caarmen/nounours-android/issues

Auto Name:Nounours
Summary:Nounours the teddy bear
Description:
This cute teddy bear is a fun friend for children of all ages. Move him around
by moving his head, paws, tummy, and ears. Spin him around.  Shake the phone.
Nounours does some funny animations (disco, flying, jumping). Record and share
his moves.
.

Repo Type:git
Repo:https://github.com/caarmen/nounours-android

Build:2.0.1,201
    commit=release-2.0.1
    subdir=app
    gradle=yes

Build:2.0.2,202
    commit=release-2.0.2
    subdir=app
    gradle=yes

Auto Update Mode:Version release-%v
Update Check Mode:Tags ^release-
Current Version:2.0.2
Current Version Code:202
