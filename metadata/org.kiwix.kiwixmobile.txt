Categories:Internet
License:GPLv3
Web Site:http://www.kiwix.org
Source Code:http://sourceforge.net/projects/kiwix
Issue Tracker:http://sourceforge.net/p/kiwix/bugs

Auto Name:Kiwix
Summary:Offline Wikipedia reader
Description:
Kiwix lets you read Wikipedia and other websites without an Internet connection.
It uses the highly compressed [http://www.openzim.org/ ZIM format] to store
webpages for quick and easy reading.
.

Repo Type:git
Repo:git://git.code.sf.net/p/kiwix/kiwix

Build:1.96,26
    disable=HEAD commit, wait for release
    commit=14bfa38fffc62074016ad93b49303d13b607de9f
    subdir=android
    gradle=yes
    rm=ios,src/sugar,android/libs/*/*.so
    build=cd .. && \
        ./autogen.sh && \
        ./configure --disable-staticbins --disable-manager --disable-server --disable-launcher --disable-indexer --disable-installer --disable-searcher --disable-reader --disable-components --enable-android --enable-compileall && \
        pushd src/dependencies && \
        make icudt49l.dat xz icu zimlib-1.2 zimlib-1.2/build/lib/libzim.so && \
        popd && \
        cd android && \
        NDK_PATH=$$NDK$$ ./build-android-with-native.py --toolchain --lzma --icu --zim --kiwix --strip

Maintainer Notes:
Don't run `make android-deps` as that will download the SDK and NDK.
.

Auto Update Mode:None
Update Check Mode:Tags android_*
Current Version:1.96
Current Version Code:26
